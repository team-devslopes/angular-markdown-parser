import { MarkdownParsePage } from './app.po';

describe('markdown-parse App', function() {
  let page: MarkdownParsePage;

  beforeEach(() => {
    page = new MarkdownParsePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
