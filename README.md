# Slope Session - Angular 2: Adding a Markdown Parser
This is the source code for an Angular tutorial that shows how to add a markdown parser (marked) to an Angular2 project. This project was created using the Angular CLI (webpack).

## Starting Out
Make sure you use the **starting-point** branch when starting the tutorial.

## Complete
The **master** branch is a copy of my code when I completed the tutorial video.

Thanks
Jess Rascal


